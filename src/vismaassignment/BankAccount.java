/* Matias Salohonka
   26.02.17
*/
package vismaassignment;

public class BankAccount {
    
    private final String shortID;
    private String longID;
    
    public BankAccount(String ID) {
        shortID = ID;
        longID = null;
    }
    
    public String getShortID() {
        return shortID;
    }
    
    public String getLongID() { //dont form longID unless asked
        if(longID == null) {
            longID = calcLongID();
        }
        return longID;
    }
    
    public String calcLongID() {
        
        String tempLongID = shortID.substring(0, 6); //capture first 6 digits
        int zeros = 14-shortID.length()+1; //how many zeros to add
        
        if(shortID.charAt(0) == ('4' | '5')) { //Sp, Pop, Aktia, Op, OKO
            tempLongID += shortID.charAt(7); //capture 7. digit
            
            for(int i=0;i<zeros;i++) {
                tempLongID += '0';
            }
            tempLongID = tempLongID.concat(shortID.substring(8)); //last digits
            
        } else { //other banks
            for(int i=0;i<zeros;i++) {
                tempLongID += '0';
            }
            tempLongID = tempLongID.concat(shortID.substring(7));
        }
        
        return tempLongID;
    }
}

/* eof */
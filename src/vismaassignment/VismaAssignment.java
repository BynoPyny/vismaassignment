/* Matias Salohonka
   26.02.17
*/
package vismaassignment;

import java.io.IOError;
import java.util.ArrayList;
import java.util.Scanner;

public class VismaAssignment {
    
    private static ArrayList<BankAccount> accounts;
    private static Scanner c;

    public static void main(String[] args) {
        accounts = new ArrayList();
        c = new Scanner(System.in);
        String choice;
        String account;
        
        while(true) {
            choice = "new round yay";
            account = "placeholder";
            System.out.print("Welcome, please choose an option:\n"
                                + "1) Enter a new bank account\n"
                                + "2) List accounts\n"
                                + "3) Generate an accounts long number format\n"
                                + "0) Exit\n"
                                + "Enter choice: ");

            try {
                choice = c.nextLine();
            } catch (IOError ex) {
                System.out.println("Error reading input");
                continue;
            }

            switch(choice) {
                case "0": //exit
                    System.exit(0);
                    break;

                case "1": //new account
                    System.out.print("Please enter the account number in short format: ");
                    try {
                        account = c.nextLine();
                    } catch (IOError ex) {
                        System.out.println("Error reading input");
                    }

                    if(checkBankAccount(account)) { //check input account formatting
                        accounts.add(new BankAccount(account));
                    } else {
                        System.out.println("Invalid account");
                    }
                    break;

                case "2": //list accounts
                    printAccounts();
                    break;

                case "3": //give long format
                    printAccounts();
                    System.out.print("Enter choice: ");
                    try {
                        choice = c.nextLine();
                    } catch (IOError ex) {
                        System.out.println("Error reading input");
                    }

                    System.out.println(accounts.get(Integer.parseInt(choice)-1).getLongID());
                    break;

                default:
                    System.out.println("Invalid choice");
                    break;
            }
            System.out.println("");
        }
    }
    
    public static boolean checkBankAccount(String account) {
        
        try { //if any operation fails, return false
            String parts[] = account.split("-");
            
            if(parts[0].length() != 6) {
                return false;
                
            } else if(parts[1].length() < 2 || parts[1].length() > 8) {
                return false;
            }
            
            int test1 = Integer.parseInt(parts[0]); //check that parts are numbers
            int test2 = Integer.parseInt(parts[1]);
            
        } catch (Exception ex) {
            return false;
        }
        return true;
    }
    
    public static void printAccounts() {
        for (int i = 1; i <= accounts.size(); i++) {
            System.out.println(i + ") " + accounts.get(i-1).getShortID());
        }
    }
    
}

/* eof */